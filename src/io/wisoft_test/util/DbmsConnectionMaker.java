package io.wisoft_test.util;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DbmsConnectionMaker {
  private static DbmsConnectionMaker dbmsConnectionMaker = null;
  private static Properties properties;

  private static String dbmsUrl;
  private static String dbmsDriver;
  private static String dbmsUsername;
  private static String dbmsPassword;

  public static Connection getConnection() {
    if (dbmsConnectionMaker == null) {
      dbmsConnectionMaker = new DbmsConnectionMaker();
    }
    return dbmsConnectionMaker.makeConnection();
  }

  private Connection makeConnection() {
    Connection connection = null;

    try {
      connection = DriverManager.getConnection(dbmsUrl, dbmsUsername, dbmsPassword);
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }

    return connection;
  }

  private DbmsConnectionMaker() {
    try {
      setupDbmsEnvironment();
      // 드라이버 정보를 JVM 에게 알려준다. (관례 라고 생각)
      Class.forName(dbmsDriver);
    } catch (final Exception e) {
      System.err.println("Exception: " + e);
    }
  }

  private void setupDbmsEnvironment() {
    extractDbmsEnviroment();
    // refactoring (cmd + alt + m) 함수 1나는 1나의 기능만 해야 되므로

    try {
      dbmsUrl = properties.getProperty("database.url");
      dbmsDriver = properties.getProperty("database.driver");
      dbmsUsername = properties.getProperty("database.username");
      dbmsPassword = properties.getProperty("database.password");
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }
  }

  private void extractDbmsEnviroment() {
    String filePath = "./resources/properties/dbms.properties";

    try {
      properties = new Properties();
      FileInputStream fileInputStream = new FileInputStream(filePath);
      properties.load(fileInputStream);
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }
  }
}
