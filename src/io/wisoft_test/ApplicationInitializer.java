package io.wisoft_test;

import io.wisoft_test.account.Account;
import io.wisoft_test.account.AccountService;
import io.wisoft_test.account.SimpleAccountService;

import java.util.List;

public class ApplicationInitializer {
  public static void main(String[] args) {

    boolean result;
    AccountService accountService = new SimpleAccountService();


    System.out.println("\nRegister Your Information");
    System.out.println("======================================================");
    Account newAccount = new Account("bhlee", "이병훈", "lbh5941@gmail.com");
    result = accountService.register(newAccount);
    checkOperationResult(result);
    System.out.println("======================================================");
    printAccounts();

    System.out.println("\nUpdate Your Information");
    System.out.println("======================================================");
    Account modifiedAccount = new Account("bhlee", "이병훈2", "lbh5942@gmail.com");
    result = accountService.modify(modifiedAccount);
    checkOperationResult(result);
    System.out.println("======================================================");
    printAccounts();

//    System.out.println("\nRemove Your Information");
//    System.out.println("======================================================");
//
//    result = accountService.remove(modifiedAccount.getUsername());
//    checkOperationResult(result);
//    printAccounts();

    System.out.println("======================================================");

    printAccount(newAccount.getUsername());
  }

  private static void printAccount(String username) {
    AccountService accountService = new SimpleAccountService();

    System.out.println("Find Account Information");
    System.out.println("======================================================");
    Account account = accountService.findByUsername(username);
    System.out.println(account.toString());
    System.out.println("======================================================");
  }

  private static void printAccounts() {
    AccountService accountService = new SimpleAccountService();

    System.out.println("\n Find All Account Information");
    System.out.println("======================================================");
    List<Account> accounts = accountService.findAll();
    accounts.forEach(System.out::println);
    System.out.println("======================================================");
  }

  private static void checkOperationResult(boolean result) {
    if (result) {
      System.out.println("success");
    } else {
      System.out.println("fail");
    }
  }
}
