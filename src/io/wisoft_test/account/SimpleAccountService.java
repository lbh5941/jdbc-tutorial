package io.wisoft_test.account;

import io.wisoft_test.util.DbmsConnectionMaker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// try resource 이해
// PreparedStatement: jdbc 에 연결되어 있는 저장소
// statement는 쓰면 안된다.

public class SimpleAccountService implements AccountService{
  @Override
  public boolean modify(Account account) {
    String query = "UPDATE account SET name = ?, email = ? WHERE username = ?";

    int result = 0;
    try (Connection connection = DbmsConnectionMaker.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(query)) {
      connection.setAutoCommit(false);

      try {
        preparedStatement.setString(1, account.getName());
        preparedStatement.setString(2, account.getEmail());
        preparedStatement.setString(3, account.getUsername());
        result = preparedStatement.executeUpdate();
        connection.commit();
      } catch (Exception e) {
        connection.rollback();
        System.err.println("Exception: " + e);
      }
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }
    return result > 0;
  }

  @Override
  public boolean register(Account account) {
    String query = "INSERT INTO account(username, name, email) VALUES (?, ?, ?)";

    int result = 0;
    try (Connection connection = DbmsConnectionMaker.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(query)) {
      connection.setAutoCommit(false);

      try {
        preparedStatement.setString(1, account.getUsername());
        preparedStatement.setString(2, account.getName());
        preparedStatement.setString(3, account.getEmail());
        result = preparedStatement.executeUpdate();
        connection.commit();
      } catch (Exception e) {
        connection.rollback();
        System.err.println("Exception: " + e);
      }
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }

    return result > 0;
  }

  @Override
  public boolean remove(String username) {
    String query = "DELETE FROM account WHERE username = ?";

    int result = 0;
    try (Connection connection = DbmsConnectionMaker.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(query)) {
      connection.setAutoCommit(false);

      try {
        preparedStatement.setString(1, username);
        result = preparedStatement.executeUpdate();
        connection.commit();
      } catch (Exception e) {
        connection.rollback();
        System.err.println("Exception: " + e);
      }
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }
    return result > 0;
  }

  @Override
  public Account findByUsername(String username) {
    Account account = null;
    String query = "SELECT username, name, email FROM account WHERE username = ?";

    try (Connection connection = DbmsConnectionMaker.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(query)) {
      connection.setAutoCommit(false);

      try {
        preparedStatement.setString(1, username);
      } catch (Exception e) {
        System.err.println("Exception: " + e);
      }

      try (ResultSet resultSet = preparedStatement.executeQuery()){
        if (!resultSet.next()) {
          throw new SQLException("No Data Found");
        }

        account = new Account(resultSet.getString("username"),
                              resultSet.getString("name"),
                              resultSet.getString("email"));
      } catch (Exception e) {
        System.err.println("Exception: " + e);
      }
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }
    return account;
  }

  @Override
  public List<Account> findAll() {
    List<Account> accounts = new ArrayList<>();
    String query = "SELECT username, name, email FROM account";

    try (Connection connection = DbmsConnectionMaker.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery()) {

      while ((resultSet.next())) {
        accounts.add(new Account(resultSet.getString("username"),
          resultSet.getString("name"),
          resultSet.getString("email")));
      }
    } catch (Exception e) {
      System.err.println("Exception: " + e);
    }

    return accounts;
  }
}
