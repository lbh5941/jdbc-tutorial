package io.wisoft_test.account;

import java.util.List;

/**
 * Created by ibyeonghun on 2017. 4. 28..
 */
public interface AccountService {
  boolean register(Account account);

  boolean modify(Account account);

  boolean remove(String username);

  Account findByUsername(String username);

  List<Account> findAll();
}
