CREATE USER study with password 'study';

CREATE DATABASE studydb ENCODING 'utf-8';
GRANT all on DATABASE studydb to study;

DROP TABLE account;
CREATE TABLE account (
  username VARCHAR(10),
  NAME     VARCHAR(10),
  email    VARCHAR(50),
  PRIMARY KEY (username)
);

insert into account (username, name, email)
VALUES ('lbh', '홍길동', 'lbh5941@gmail.com'),
       ('lbh2', '일지매', 'lbh5942@gmail.com'),
       ('lbh3', '활진이', 'lbh5943@gmail.com');